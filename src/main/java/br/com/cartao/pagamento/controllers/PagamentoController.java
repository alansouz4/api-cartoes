package br.com.cartao.pagamento.controllers;

import br.com.cartao.pagamento.models.Pagamento;
import br.com.cartao.pagamento.models.mappers.PagamentoMapper;
import br.com.cartao.pagamento.models.requests.PagamentoRequest;
import br.com.cartao.pagamento.models.responses.PagamentoResponse;
import br.com.cartao.pagamento.services.PagamentoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class PagamentoController {

    private final PagamentoService pagamentoService;
    private final PagamentoMapper mapper;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoResponse salvarPagamento(@RequestBody @Valid PagamentoRequest pagamentoRequest) {

        Pagamento pagamento = mapper.toPagamento(pagamentoRequest);

        pagamento = pagamentoService.realizarPagamento(pagamento);

        return mapper.toPagamentoResponse(pagamento);
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public List<Pagamento> listaPagamentoPorCartao(@PathVariable int id_cartao) {
        return pagamentoService.buscarPagamentosPorCartao(id_cartao);
    }
}
