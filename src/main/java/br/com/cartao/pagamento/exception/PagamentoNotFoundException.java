package br.com.cartao.pagamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Cartão não existe")
public class PagamentoNotFoundException extends RuntimeException {
}
