package br.com.cartao.pagamento.gateways;

import br.com.cartao.pagamento.models.Pagamento;

import java.util.List;

public interface PagamentoGateway {

        Pagamento realizarPagamento(Pagamento pagamento);
        List<Pagamento> buscarPagamentosPorCartao(int id_cartao);
}
