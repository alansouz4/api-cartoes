package br.com.cartao.pagamento.models.responses;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PagamentoResponse {

    private int id;
    private int cartao_id;
    private String descricao;
    private BigDecimal valor;

}
