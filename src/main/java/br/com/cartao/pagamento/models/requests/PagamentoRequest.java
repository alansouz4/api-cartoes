package br.com.cartao.pagamento.models.requests;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PagamentoRequest {

    private int id;
    private int cartao_id;
    private String descricao;
    private BigDecimal valor;

}
