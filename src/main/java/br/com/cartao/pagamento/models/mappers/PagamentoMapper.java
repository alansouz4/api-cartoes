package br.com.cartao.pagamento.models.mappers;

import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.pagamento.models.Pagamento;
import br.com.cartao.pagamento.models.requests.PagamentoRequest;
import br.com.cartao.pagamento.models.responses.PagamentoResponse;
import org.springframework.stereotype.Component;

@Component
public class PagamentoMapper {

    public Pagamento toPagamento(PagamentoRequest pagamentoRequest) {

        Cartao cartao = new Cartao();
        cartao.setId(pagamentoRequest.getCartao_id());

        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(pagamentoRequest.getDescricao());
        pagamento.setValor(pagamentoRequest.getValor());

        pagamento.setCartao(cartao);

        return pagamento;
    }

    public PagamentoResponse toPagamentoResponse(Pagamento pagamento) {

        PagamentoResponse pagamentoResponse = new PagamentoResponse();

        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartao_id(pagamento.getCartao().getId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());

        return pagamentoResponse;
    }
    

}
