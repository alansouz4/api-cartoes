package br.com.cartao.pagamento.models;

import br.com.cartao.cartao.models.Cartao;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull(message = "Cartão é obrigatório")
    private Cartao cartao;

    @NotNull(message = "Descrição é obrigatório")
    private String descricao;

    @NotNull(message = "Valor é obrigatório")
    private BigDecimal valor;
}
