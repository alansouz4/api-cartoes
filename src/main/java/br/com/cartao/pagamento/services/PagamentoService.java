package br.com.cartao.pagamento.services;

import br.com.cartao.cartao.services.CartaoService;
import br.com.cartao.pagamento.exception.PagamentoNotFoundException;
import br.com.cartao.pagamento.gateways.PagamentoGateway;
import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.pagamento.models.Pagamento;
import br.com.cartao.pagamento.respositories.PagamentoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PagamentoService implements PagamentoGateway {

    private final PagamentoRepository pagamentoRepository;
    private final CartaoService cartaoService;

    @Override
    public Pagamento realizarPagamento(Pagamento pagamento) {
        Cartao cartao = cartaoService.buscarCartaoPorId(pagamento.getCartao().getId());

        if(cartao != null) {
            pagamento.setCartao(cartao);

            return pagamentoRepository.save(pagamento);
        }
        throw new PagamentoNotFoundException();
    }

    @Override
    public List<Pagamento> buscarPagamentosPorCartao(int id_cartao) {
        return pagamentoRepository.findAllByCartaoId(id_cartao);
    }
}
