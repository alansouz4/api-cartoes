package br.com.cartao.pagamento.respositories;

import br.com.cartao.pagamento.models.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PagamentoRepository extends JpaRepository<Pagamento, Integer> {

    List<Pagamento> findAllByCartaoId(int id_cartao);
}
