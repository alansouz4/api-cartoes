package br.com.cartao.cliente.gateways;

import br.com.cartao.cliente.models.Cliente;

public interface ClienteGateway {

    Cliente salvaCliente(Cliente cliente);
    Cliente listaClientePorId(int id);
}
