package br.com.cartao.cliente.controllers;

import br.com.cartao.cliente.exceptions.ClienteNotFoundException;
import br.com.cartao.cliente.models.Cliente;
import br.com.cartao.cliente.services.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
@RequiredArgsConstructor
public class ClienteController {

    private final ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> salvarCliente(@RequestBody @Valid Cliente cliente) {
        return new ResponseEntity(clienteService.salvaCliente(cliente), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Cliente> listarClientePorId(@PathVariable(name = "id")  int id){
        try{
            return ResponseEntity.ok(clienteService.listaClientePorId(id));
        } catch (RuntimeException e) {
            throw new ClienteNotFoundException();
        }

    }
}
