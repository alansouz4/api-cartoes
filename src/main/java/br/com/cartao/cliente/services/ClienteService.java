package br.com.cartao.cliente.services;

import br.com.cartao.cliente.exceptions.ClienteNotFoundException;
import br.com.cartao.cliente.gateways.ClienteGateway;
import br.com.cartao.cliente.models.Cliente;
import br.com.cartao.cliente.repositories.ClienteRespository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ClienteService implements ClienteGateway {

    private final ClienteRespository clienteRespository;

    @Override
    public Cliente salvaCliente(Cliente cliente) {
        return clienteRespository.save(cliente);
    }

    @Override
    public Cliente listaClientePorId(int id) {
        Optional<Cliente> cliente = clienteRespository.findById(id);

        if(!cliente.isPresent()){
            throw new ClienteNotFoundException();
        }

        return cliente.get();
    }
}
