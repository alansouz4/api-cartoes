package br.com.cartao.cliente.models.dtos;

import lombok.Data;

@Data
public class CriaClienteRequest {

    private String nome;
}
