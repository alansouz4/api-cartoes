package br.com.cartao.cartao.services;

import br.com.cartao.cartao.exception.CartaoNotFoundException;
import br.com.cartao.cartao.gateways.CartaoGateway;
import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cliente.models.Cliente;
import br.com.cartao.cartao.repositories.CartaoRepository;
import br.com.cartao.cliente.services.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CartaoService implements CartaoGateway {

    private final CartaoRepository cartaoRepository;
    private final ClienteService clienteService;

    @Override
    public Cartao criaCartao(Cartao cartao){
        Cliente cliente = clienteService.listaClientePorId(cartao.getCliente().getId());

        if (cliente != null) {

            cartao.setCliente(cliente);
            cartao.setAtivo(false);

            return cartaoRepository.save(cartao);

        } else {
            throw new CartaoNotFoundException();
        }
    }

    @Override
    public Cartao buscarCartaoPorNumero(String numero) {
        Optional<Cartao> cartao = cartaoRepository.findByNumero(numero);

        if (cartao.isPresent()) {

            return cartao.get();
        }
        throw new CartaoNotFoundException();
    }
    @Override
    public Cartao ativarCartao(Cartao cartao) {

        Cartao databaseCartao = buscarCartaoPorNumero(cartao.getNumero());

        databaseCartao.setAtivo(cartao.getAtivo());

        return cartaoRepository.save(databaseCartao);

    }

    @Override
    public Cartao buscarCartaoPorId(int id) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);
        if (optionalCartao.isPresent()) {
            return optionalCartao.get();
        }
        throw new CartaoNotFoundException();
    }
}
