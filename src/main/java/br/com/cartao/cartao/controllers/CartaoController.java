package br.com.cartao.cartao.controllers;

import br.com.cartao.cartao.exception.CartaoNotFoundException;
import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cartao.models.mappers.CartaoMapper;
import br.com.cartao.cartao.models.request.CartaoAtivarRequest;
import br.com.cartao.cartao.models.request.CartaoRequest;
import br.com.cartao.cartao.models.response.CartaoAtivoReponse;
import br.com.cartao.cartao.models.response.CartaoGetReponse;
import br.com.cartao.cartao.models.response.CartaoResponse;
import br.com.cartao.cartao.services.CartaoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
@RequiredArgsConstructor
public class CartaoController {

    private final CartaoService cartaoService;
    private final CartaoMapper cartaoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoResponse salvaCartao(@RequestBody @Valid CartaoRequest cartaoRequest) {

        Cartao cartao = cartaoMapper.toCartao(cartaoRequest);
        cartao = cartaoService.criaCartao(cartao);

        return cartaoMapper.toCartaoResponse(cartao);
    }

    @PatchMapping("/{numero}")
    public CartaoAtivoReponse ativarCartao(@PathVariable(name = "numero") String numero,
                                           @RequestBody CartaoAtivarRequest cartaoAtivarRequest){

        try{
            cartaoAtivarRequest.setNumero(numero);
            Cartao cartao = cartaoMapper.toCartao(cartaoAtivarRequest);

            cartao = cartaoService.ativarCartao(cartao);

            return cartaoMapper.toCartaoAtivoResponse(cartao);

        }catch(CartaoNotFoundException exception){
            throw  new CartaoNotFoundException();
        }
    }

    @GetMapping("/{numero}")
    public CartaoGetReponse buscarCartaoPorNumero(@PathVariable(name = "numero") String numero) {
        try{
            Cartao cartao = cartaoService.buscarCartaoPorNumero(numero);

            return cartaoMapper.toCartaoGetReponse(cartao);

        } catch (CartaoNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
