package br.com.cartao.cartao.repositories;

import br.com.cartao.cartao.models.Cartao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartaoRepository extends JpaRepository<Cartao, Integer> {

    Optional<Cartao> findByNumero(String numero);
}
