package br.com.cartao.cartao.gateways;

import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cartao.models.request.CartaoAtivarRequest;

public interface CartaoGateway {

    Cartao criaCartao(Cartao cartao);
    Cartao buscarCartaoPorNumero(String numero);
    Cartao ativarCartao(Cartao cartao);
    Cartao buscarCartaoPorId(int id);
}
