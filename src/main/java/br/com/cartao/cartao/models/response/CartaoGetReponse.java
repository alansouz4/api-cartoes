package br.com.cartao.cartao.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CartaoGetReponse {

    private int id;

    @JsonProperty("numero")
    private String numero;

    @JsonProperty("clienteId")
    private int clienteId;

}
