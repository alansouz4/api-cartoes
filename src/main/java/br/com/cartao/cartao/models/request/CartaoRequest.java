package br.com.cartao.cartao.models.request;

import lombok.Data;

@Data
public class CartaoRequest {

    private int id;
    private String numero;
    private Integer clienteId;
}

