package br.com.cartao.cartao.models.response;

import lombok.Data;

@Data
public class CartaoResponse {

    private int id;
    private String numero;
    private int clienteId;
    private boolean ativo;

}
