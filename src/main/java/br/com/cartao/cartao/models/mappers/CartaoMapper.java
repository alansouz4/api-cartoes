package br.com.cartao.cartao.models.mappers;

import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cartao.models.request.CartaoAtivarRequest;
import br.com.cartao.cartao.models.request.CartaoRequest;
import br.com.cartao.cartao.models.response.CartaoAtivoReponse;
import br.com.cartao.cartao.models.response.CartaoGetReponse;
import br.com.cartao.cartao.models.response.CartaoResponse;
import br.com.cartao.cliente.models.Cliente;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao(CartaoRequest cartaoRequest) {

        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoRequest.getNumero());

        Cliente cliente = new Cliente();
        cliente.setId(cartaoRequest.getClienteId());

        cartao.setCliente(cliente);

        return cartao;
    }

    public CartaoResponse toCartaoResponse(Cartao cartao) {
        CartaoResponse cartaoResponse = new CartaoResponse();

        cartaoResponse.setId(cartao.getId());
        cartaoResponse.setNumero(cartao.getNumero());
        cartaoResponse.setClienteId(cartao.getCliente().getId());
        cartaoResponse.setAtivo(cartao.getAtivo());

        return  cartaoResponse;
    }

    public Cartao toCartao(CartaoAtivarRequest cartaoAtivarRequest) {
        Cartao cartao = new Cartao();

        cartao.setNumero(cartaoAtivarRequest.getNumero());
        cartao.setAtivo(cartaoAtivarRequest.getAtivo());

        return cartao;
    }

    public CartaoAtivoReponse toCartaoAtivoResponse(Cartao cartao) {
        CartaoAtivoReponse cartaoAtivoReponse = new CartaoAtivoReponse();

        cartaoAtivoReponse.setId(cartao.getId());
        cartaoAtivoReponse.setNumero(cartao.getNumero());
        cartaoAtivoReponse.setClienteId(cartao.getCliente().getId());
        cartaoAtivoReponse.setAtivo(cartao.getAtivo());

        return cartaoAtivoReponse;
    }

    public CartaoGetReponse toCartaoGetReponse(Cartao cartao) {
        CartaoGetReponse cartaoGetReponse = new CartaoGetReponse();

        cartaoGetReponse.setId(cartao.getId());
        cartaoGetReponse.setNumero(cartao.getNumero());
        cartaoGetReponse.setClienteId(cartao.getCliente().getId());

        return cartaoGetReponse;
    }
}
