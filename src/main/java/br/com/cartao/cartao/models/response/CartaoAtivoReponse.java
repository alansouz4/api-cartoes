package br.com.cartao.cartao.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CartaoAtivoReponse {

    private int id;

    @JsonProperty("numero")
    private String numero;

    @JsonProperty("clienteId")
    private int clienteId;

    @JsonProperty("ativo")
    private Boolean ativo;
}
