package br.com.cartao.cartao.models;

import br.com.cartao.cliente.models.Cliente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Número cartão é obrigatório")
    @Column(unique = true)
    private String numero;

    @ManyToOne
    private Cliente cliente;

    @Column
    private Boolean ativo;
}
